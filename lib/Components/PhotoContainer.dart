import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PhotoContainer extends StatelessWidget {
  final String title;
  final bool check;
  final dynamic onTap;
  final bool photoCheck;
  final profileUrl;
  const PhotoContainer({
    Key? key,
    required this.photoCheck,
    required this.profileUrl,
    required this.check,
    required this.title,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10, bottom: 5, top: 20),
          child: Text(
            title,
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.red[700]),
          ),
        ),
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.only(right: check ? 100 : 0),
            child: Container(
              height: 200,
              alignment: Alignment.center,
              decoration: photoCheck
                  ? BoxDecoration(
                      image: DecorationImage(
                        image: FileImage(File(profileUrl.path)),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.grey.withOpacity(0.3)))
                  : BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.grey.withOpacity(0.3))),
              child: photoCheck
                  ? SizedBox()
                  : Icon(
                      Icons.add,
                      color: Colors.grey,
                      size: 30,
                    ),
            ),
          ),
        ),
      ],
    );
  }
}
