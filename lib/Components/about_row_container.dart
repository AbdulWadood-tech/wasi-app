import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutRowContents extends StatelessWidget {
  final String title;
  final IconData iconData;
  final String leading;
  const AboutRowContents({
    Key? key,
    required this.leading,
    required this.iconData,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 12),
      child: Row(
        children: [
          Icon(
            iconData,
            color: Colors.grey,
            size: 18,
          ),
          SizedBox(width: 10),
          Text(leading),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
