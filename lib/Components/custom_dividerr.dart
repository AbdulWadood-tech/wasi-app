import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 70),
      child: Container(
        width: double.infinity,
        height: 1,
        color: Colors.grey.withOpacity(0.3),
      ),
    );
  }
}
