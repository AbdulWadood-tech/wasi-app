import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final IconData? prefixIconData;
  final IconData? suffixIcon;
  final dynamic function;
  final bool check;
  final dynamic onChanged;
  final dynamic validation;
  const CustomTextField({
    Key? key,
    this.validation,
    required this.onChanged,
    this.check = false,
    this.function,
    required this.hint,
    this.prefixIconData,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: TextFormField(
        validator: validation,
        onChanged: onChanged,
        obscureText: check ? true : false,
        style: TextStyle(color: Colors.blue),
        decoration: InputDecoration(
          fillColor: themeChanger.textFieldColor,
          filled: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: themeChanger.borderColor, width: 0.0),
            borderRadius: BorderRadius.circular(10),
          ),
          prefixIcon: prefixIconData != null
              ? Icon(
                  prefixIconData,
                  color: themeChanger.textColor,
                  size: 20,
                )
              : null,
          suffixIcon: IconButton(
              onPressed: function,
              icon: Icon(
                suffixIcon,
                color: check ? themeChanger.textColor : Colors.blue,
              )),
          hintText: hint,
          hintStyle: TextStyle(
            color: themeChanger.textColor,
          ),
        ),
      ),
    );
  }
}
