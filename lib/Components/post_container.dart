import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PostContainer extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String profileUrl;
  final bool check;
  final Color color;
  final dynamic function;
  final String description;
  PostContainer({
    required this.imageUrl,
    required this.name,
    required this.color,
    required this.profileUrl,
    required this.description,
    this.check = false,
    required this.function,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 0),
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 0.5,
                blurRadius: 5,
              )
            ]),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('images/profile.jpg'),
                  radius: 25,
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Row(
                      children: [
                        Text(
                          'Business Account',
                          style: TextStyle(fontSize: 13, color: Colors.grey),
                        ),
                        SizedBox(width: 5),
                        Container(
                          alignment: Alignment.center,
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                            color: Color(0xff27AE60),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 13,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Spacer(),
                Icon(
                  Icons.list,
                  color: Colors.grey,
                )
              ],
            ),
            SizedBox(height: 20),
            Text(
              description,
            ),
            SizedBox(height: 10),
            // Container(
            //   width: double.infinity,
            //   height: 200,
            //   decoration: BoxDecoration(
            //       image: DecorationImage(
            //           image: AssetImage(imageUrl), fit: BoxFit.cover),
            //       borderRadius: BorderRadius.circular(5),
            //       boxShadow: [
            //         BoxShadow(
            //           color: Colors.grey.withOpacity(0.3),
            //           offset: Offset(0, 0),
            //           spreadRadius: 1,
            //           blurRadius: 1,
            //         ),
            //       ]),
            // ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Icon(
                    Icons.thumb_up,
                    color: Colors.blue,
                    size: 20,
                  ),
                  Text(
                    '  330',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Text(
                    '17 Comments',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Text(
                    '17 Shares',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 50),
              child: Container(
                width: double.infinity,
                height: 1,
                color: Colors.grey.withOpacity(0.4),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  InkWell(
                    onTap: function,
                    child: Icon(
                      check ? Icons.thumb_up : Icons.thumb_up_alt_outlined,
                      size: 20,
                      color: check ? Colors.blue : Colors.grey,
                    ),
                  ),
                  Text(
                    '  Like',
                    style: TextStyle(color: check ? Colors.blue : Colors.grey),
                  ),
                  Spacer(),
                  Icon(
                    Icons.mode_comment_outlined,
                    size: 20,
                    color: Colors.grey,
                  ),
                  Text(
                    '  Comments',
                    style: TextStyle(color: Colors.grey),
                  ),
                  Spacer(),
                  Icon(
                    Icons.subdirectory_arrow_right_sharp,
                    color: Colors.grey,
                    size: 20,
                  ),
                  Text(
                    'Share',
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
