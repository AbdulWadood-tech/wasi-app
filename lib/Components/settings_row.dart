import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingRow extends StatelessWidget {
  final String title;
  final String subTitle;
  final IconData icon;
  final dynamic function;
  const SettingRow({
    Key? key,
    required this.function,
    required this.title,
    required this.subTitle,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Icon(
            icon,
            color: Colors.grey,
            size: 18,
          ),
          SizedBox(width: 5),
          Text(subTitle),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Spacer(),
          InkWell(
            onTap: function,
            child: Icon(
              Icons.edit,
              color: Colors.blue,
              size: 18,
            ),
          )
        ],
      ),
    );
  }
}
