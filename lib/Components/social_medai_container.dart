import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SocialMediaContainers extends StatelessWidget {
  final String title;
  final Color? color;
  final IconData iconData;
  final dynamic onTap;
  SocialMediaContainers(
      {required this.onTap,
      required this.title,
      required this.color,
      required this.iconData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 7,
      ),
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 2),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    iconData,
                    color: Colors.white,
                    size: 20,
                  )),
              Text(
                title,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.5),
              )
            ],
          ),
        ),
      ),
    );
  }
}
