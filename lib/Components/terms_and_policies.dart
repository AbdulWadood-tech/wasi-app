import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TermsAndPoliciesText extends StatelessWidget {
  const TermsAndPoliciesText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: 'By clicking the ',
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.w300,
              )),
          TextSpan(
            text: 'Register ',
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.w300,
            ),
          ),
          TextSpan(
            text: 'button, you agreed to\nour terms and policies',
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}
