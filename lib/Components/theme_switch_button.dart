import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class ThemeSwitchButton extends StatelessWidget {
  final dynamic function;
  final bool check;
  final bool size;
  const ThemeSwitchButton({
    Key? key,
    this.size = true,
    required this.function,
    required this.check,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterSwitch(
      width: size ? 70.0 : 60,
      height: size ? 45.0 : 30,
      toggleSize: 25.0,
      value: check,
      borderRadius: 30.0,
      padding: 2.0,
      activeToggleColor: Color(0xFF2F363D),
      inactiveToggleColor: Color(0xFF6E40C9),
      activeSwitchBorder: Border.all(
        color: Color(0xFFD1D5DA),
        width: size ? 6.0 : 4,
      ),
      inactiveSwitchBorder: Border.all(
        color: Color(0xFF3C1E70),
        width: size ? 6.0 : 4,
      ),
      activeColor: Colors.white,
      inactiveColor: Color(0xFF271052),
      activeIcon: Icon(
        Icons.wb_sunny,
        color: Color(0xFFFFDF5D),
      ),
      inactiveIcon: Icon(
        Icons.nightlight_round,
        color: Color(0xFFF8E3A1),
      ),
      onToggle: function,
    );
  }
}
