import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:wasi/Models/video_model.dart';

class DatabaseServices {
  //reference for users
  final CollectionReference userReference =
      FirebaseFirestore.instance.collection('Users');

  //add user
  Future addUser(
    String profileUrl,
    String coverUrl,
    String fullName,
    String email,
    String age,
    String number,
    String address,
    String accountType,
    String businessType,
    String gender,
    String status,
    String interestedIn,
    String uid,
    String work,
  ) async {
    return await userReference.doc(uid).set({
      'Profile Url': profileUrl,
      'Cover Url': coverUrl,
      'Name': fullName,
      'Email': email,
      'Age': age,
      'Number': number,
      'Address': address,
      'Account Type': accountType,
      'Business Type': businessType,
      'Gender': gender,
      'Status': status,
      'Interested In': interestedIn,
      'Work': work,
    });
  }

  //pick file from gallery
  Future selectFile() async {
    final _picker = ImagePicker();
    var image;
    image = await _picker.pickImage(source: ImageSource.gallery);
    return image;
  }

  //upload image to storage
  Future addPhotoTStorage(File file) async {
    final storage = FirebaseStorage.instance;
    var snapshot = await storage.ref().child(file.path).putFile(file);
    String url = await snapshot.ref.getDownloadURL();
    print(url);
    return url;
  }

  //check user if exist
  Future checkUser(String uid) {
    return userReference.doc(uid).get();
  }

  //get User Stream
  Stream<DocumentSnapshot> getUserData(String uid) {
    return userReference.doc(uid).snapshots();
  }

  //check if the user completed the form
  /// Check If Document Exists
  Future<bool> checkIfDocExists(String docId) async {
    try {
      var doc = await userReference.doc(docId).get();
      return doc.exists;
    } catch (e) {
      throw e;
    }
  }

  //change profile picture
  Future changeProfilePicture(String url, String uid) {
    return userReference.doc(uid).update({'Profile Url': url});
  }

  //change Cover picture
  Future changeCoverPicture(String url, String uid) {
    return userReference.doc(uid).update({'Cover Url': url});
  }

  //change field in profile settings
  Future changeFieldInProfileSettings(String uid, String field, String value) {
    return userReference.doc(uid).update({field: value});
  }
}
