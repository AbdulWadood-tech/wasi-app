import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fluttericon/meteocons_icons.dart';
import 'package:image_picker/image_picker.dart';

import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:wasi/Models/video_model.dart';

class PostServices {
  //reference for users
  final CollectionReference postReference =
      FirebaseFirestore.instance.collection('Posts');

  //get video from gallery
  Future pickVideo() async {
    final _picker = ImagePicker();
    var video = await _picker.pickVideo(source: ImageSource.gallery);
    //
    // final getThumbnail = await VideoThumbnail.thumbnailData(
    //   video: video!.path,
    //   imageFormat: ImageFormat.JPEG,
    //   maxWidth:
    //       128, // specify the width of the thumbnail, let the height auto-scaled to keep the source aspect ratio
    //   quality: 25,
    // );
    // print(getThumbnail);
    // VideoModel model = VideoModel(video: video, videoThumbnail: getThumbnail);
    return video;
  }

  //upload video  to storage
  Future addVideoToStorage(File file) async {
    final storage = FirebaseStorage.instance;
    var snapshot = await storage.ref().child(file.path).putFile(file);
    String url = await snapshot.ref.getDownloadURL();
    print(url);
    return url;
  }

  //add post to their categories
  Future addPost(
    String uid,
    String profileUrl,
    String accType,
    String name,
    String description,
    String videoUrl,
  ) async {
    return await postReference.doc(uid).set({
      'Uid': uid,
      'Profile Url': profileUrl,
      'Account Type': accType,
      'Name': name,
      'Description': description,
      'Video Url': videoUrl,
    });
  }

  //posts streams
  Stream<QuerySnapshot> getPostStream() {
    return postReference.snapshots();
  }
}
