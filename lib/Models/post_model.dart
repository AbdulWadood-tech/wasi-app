class PostModel {
  final String name;
  final String profileUrl;
  final String accType;
  final String description;
  final String videoUrl;
  PostModel(
      {required this.description,
      required this.name,
      required this.profileUrl,
      required this.accType,
      required this.videoUrl});
}
