class UserDataModel {
  final String profileUrl;
  final String name;
  final String accountType;
  UserDataModel(
      {required this.accountType,
      required this.profileUrl,
      required this.name});
}
