class VideoModel {
  final dynamic video;
  final dynamic videoThumbnail;
  VideoModel({required this.video, required this.videoThumbnail});
}
