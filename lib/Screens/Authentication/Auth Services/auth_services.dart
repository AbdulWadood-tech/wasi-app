import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:wasi/Screens/Authentication/google_modle.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class AuthServices {
  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn();
  FacebookAuth _fAuth = FacebookAuth.instance;

  //create user with email and password
  Future createUserWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);
      User? user = userCredential.user;
      return user;
    } catch (e) {
      print(e.toString());
      print("error");
      return null;
    }
  }

  //Sign in user with existing email and password
  Future signInUserWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User? user = userCredential.user;
      return user;
    } catch (e) {
      print(e);
      return null;
    }
  }

  //sign in using google
  Future signInWithGoogle() async {
    GoogleModel model;
    try {
      final user = await _googleSignIn.signIn();
      if (user != null) {
        final googleAuth = await user.authentication;
        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        String email = user.email;
        final list =
            await FirebaseAuth.instance.fetchSignInMethodsForEmail(email);
        final result = await _auth.signInWithCredential(credential);
        User? authUser = result.user;
        if (list.isNotEmpty) {
          model = GoogleModel(check: true, user: authUser);
        } else {
          model = GoogleModel(check: false, user: authUser);
        }
        return model;
      }
    } catch (e) {
      print(e.toString());
    }
  }

  //get uid
  getUid() {
    return _auth.currentUser!.uid;
  }

  //get user
  bool getUser() {
    if (_auth.currentUser != null) {
      return true;
    } else {
      return false;
    }
  }

  //get email
  getEmail() {
    return _auth.currentUser!.email;
  }

  //logOut
  logOut() async {
    try {
      await _auth.signOut();
      return true;
    } catch (e) {
      return false;
    }
  }

  //sign in using facebook
  // Future signInWithFacebook() async {
  //   try {
  //     final result =
  //         await _fAuth.login(permissions: ['email', 'public_profile']);
  //     switch (result.status) {
  //       case LoginStatus.success:
  //         print('Succeed');
  //         final AuthCredential facebookCredential =
  //             FacebookAuthProvider.credential(result.accessToken!.token);
  //         var fData = await _fAuth.getUserData();
  //         String email = fData['email'];
  //         print(email);
  //         print(email);
  //         print(email);
  //         final userCredential =
  //             await _auth.signInWithCredential(facebookCredential);
  //         User? user = userCredential.user;
  //         return user;
  //     }
  //   } catch (e) {
  //     print(e.toString());
  //   }
  // }
  //sign in using facebook
  Future signInWithFacebook() async {
    try {
      final result =
          await _fAuth.login(permissions: ['email', 'public_profile']);
      switch (result.status) {
        case LoginStatus.cancelled:
          return null;
        case LoginStatus.success:
          print('Succeed');
          final AuthCredential facebookCredential =
              FacebookAuthProvider.credential(result.accessToken!.token);
          final userCredential =
              await _auth.signInWithCredential(facebookCredential);
          User? user = userCredential.user;
          return user;
        case LoginStatus.failed:
          return null;

        case LoginStatus.operationInProgress:
          return null;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
