import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Components/custom_text_field.dart';
import 'package:wasi/Components/registor_button.dart';
import 'package:wasi/Components/social_medai_container.dart';
import 'package:wasi/Components/terms_and_policies.dart';
import 'package:wasi/Components/theme_switch_button.dart';
import 'package:wasi/Screens/navigator_screen.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

import '../profile_form_screen.dart';
import 'Auth Services/auth_services.dart';

class SignIn extends StatefulWidget {
  final dynamic function;
  const SignIn({Key? key, required this.function}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String? errorMessage;
  bool check = true;
  bool switchState = true;
  String? userName;
  String? email;
  String? password;
  bool scroll = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final height = MediaQuery.of(context).size.height;
    return scroll
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                color: Colors.red[700],
              ),
            ),
          )
        : Scaffold(
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 60),
                      InkWell(
                        onTap: () {},
                        child: Text(
                          'Sign in',
                          style: TextStyle(
                              color: themeChanger.headingColors,
                              fontSize: 40,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 20),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your email'
                                  : null,
                              hint: 'Enter Email',
                              prefixIconData: Icons.email,
                              onChanged: (val) {
                                email = val;
                              },
                            ),
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your password'
                                  : null,
                              hint: 'Enter Password',
                              prefixIconData: Icons.lock_sharp,
                              suffixIcon: check
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              check: check,
                              function: () {
                                setState(() {
                                  check = !check;
                                });
                              },
                              onChanged: (val) {
                                password = val;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sign In',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          RegisterButton(function: () async {
                            if (_formKey.currentState!.validate()) {
                              setState(() {
                                scroll = true;
                              });
                              final user = await AuthServices()
                                  .signInUserWithEmailAndPassword(
                                      email!, password!);
                              if (user != null) {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return NavigatorScreen();
                                }));
                              }
                              setState(() {
                                scroll = false;
                                errorMessage = 'Wrong password or email';
                              });
                            }
                          }),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.19,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            errorMessage ?? '',
                            style: TextStyle(
                                color: Colors.red[700],
                                fontWeight: FontWeight.bold),
                          ),
                          errorMessage == null
                              ? SizedBox()
                              : SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 60,
                                child: Divider(
                                  color: Colors.grey.withOpacity(0.5),
                                  thickness: 1,
                                ),
                              ),
                              SizedBox(width: 5),
                              Text(
                                'OR',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(width: 5),
                              SizedBox(
                                width: 60,
                                child: Divider(
                                  color: Colors.grey.withOpacity(0.5),
                                  thickness: 1,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          SocialMediaContainers(
                            onTap: () {
                              print('doingggg');
                            },
                            title: 'Connect with Facebook',
                            color: Colors.blue[800],
                            iconData: FontAwesome.facebook,
                          ),
                          SocialMediaContainers(
                            onTap: () async {
                              setState(() {
                                scroll = true;
                              });
                              final user =
                                  await AuthServices().signInWithGoogle();
                              if (user != null) {
                                final checkEmail = user.check;
                                if (checkEmail == true) {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return NavigatorScreen();
                                  }));
                                } else {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return ProfileFormScreen(
                                      email: user.user.email,
                                    );
                                  }));
                                }
                              }
                              setState(() {
                                scroll = false;
                                errorMessage = 'Wrong Credential';
                              });
                            },
                            title: 'Connect with Google',
                            color: Colors.red[800],
                            iconData: FontAwesome.google,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.078,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Don\'t have an account? ',
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          InkWell(
                            onTap: this.widget.function,
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                color: Colors.deepOrangeAccent,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
