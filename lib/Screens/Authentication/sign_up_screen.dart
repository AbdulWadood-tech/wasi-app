import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Components/custom_text_field.dart';
import 'package:wasi/Components/registor_button.dart';
import 'package:wasi/Components/social_medai_container.dart';
import 'package:wasi/Components/terms_and_policies.dart';
import 'package:wasi/Components/theme_switch_button.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/navigator_screen.dart';
import 'package:wasi/Screens/profile_form_screen.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

class SignUp extends StatefulWidget {
  final dynamic function;
  const SignUp({Key? key, required this.function}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String? errorMessage;
  bool passwordHide = true;
  // bool switchState = true;
  String? userName;
  String? email;
  String? password;
  bool scroll = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final height = MediaQuery.of(context).size.height;
    return scroll
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                color: Colors.red[700],
              ),
            ),
          )
        : Scaffold(
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Create an\naccount',
                            style: TextStyle(
                                color: themeChanger.headingColors,
                                fontSize: 40,
                                fontWeight: FontWeight.bold),
                          ),
                          ThemeSwitchButton(
                            check: themeChanger.themeValue,
                            function: (val) {
                              setState(() {
                                themeChanger.themeValue = val;
                                themeChanger
                                    .changeTheme(themeChanger.themeValue);
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your username'
                                  : null,
                              hint: 'Enter Username',
                              prefixIconData: Icons.person,
                              onChanged: (val) {
                                userName = val;
                              },
                            ),
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your email'
                                  : null,
                              hint: 'Enter Email',
                              prefixIconData: Icons.email,
                              onChanged: (val) {
                                email = val;
                              },
                            ),
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your password'
                                  : null,
                              hint: 'Enter Password',
                              prefixIconData: Icons.lock_sharp,
                              suffixIcon: passwordHide
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              check: passwordHide,
                              function: () {
                                setState(() {
                                  passwordHide = !passwordHide;
                                });
                              },
                              onChanged: (val) {
                                password = val;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      TermsAndPoliciesText(),
                      SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Register',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          RegisterButton(function: () async {
                            if (_formKey.currentState!.validate()) {
                              setState(() {
                                scroll = true;
                              });
                              final user = await AuthServices()
                                  .createUserWithEmailAndPassword(
                                      email!, password!);
                              if (user != null) {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return ProfileFormScreen(
                                    email: email!,
                                  );
                                }));
                              }
                              setState(() {
                                scroll = false;
                                errorMessage = 'Wrong Credential';
                              });
                            }
                          }),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.09,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            errorMessage ?? '',
                            style: TextStyle(
                                color: Colors.red[700],
                                fontWeight: FontWeight.bold),
                          ),
                          errorMessage == null
                              ? SizedBox()
                              : SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 60,
                                child: Divider(
                                  color: Colors.grey.withOpacity(0.5),
                                  thickness: 1,
                                ),
                              ),
                              SizedBox(width: 5),
                              Text(
                                'OR',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(width: 5),
                              SizedBox(
                                width: 60,
                                child: Divider(
                                  color: Colors.grey.withOpacity(0.5),
                                  thickness: 1,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          SocialMediaContainers(
                            onTap: () async {
                              print('gg');
                              setState(() {
                                scroll = true;
                              });
                              print('doing');
                              final user =
                                  await AuthServices().signInWithFacebook();
                              print(user);
                              print(user);
                              if (user != null) {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return ProfileFormScreen(
                                    email: user.user.email,
                                  );
                                }));
                              }
                              setState(() {
                                scroll = false;
                                errorMessage = 'Wrong Credential';
                              });
                            },
                            title: 'Connect with Facebook',
                            color: Colors.blue[800],
                            iconData: FontAwesome.facebook,
                          ),
                          SocialMediaContainers(
                            onTap: () async {
                              setState(() {
                                scroll = true;
                              });
                              final user =
                                  await AuthServices().signInWithGoogle();
                              if (user != null) {
                                final checkEmail = user.check;
                                if (checkEmail == true) {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return NavigatorScreen();
                                  }));
                                } else {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return ProfileFormScreen(
                                      email: user.user.email,
                                    );
                                  }));
                                }
                              }
                              setState(() {
                                scroll = false;
                                errorMessage = 'Wrong Credential';
                              });
                            },
                            title: 'Connect with Google',
                            color: Colors.red[800],
                            iconData: FontAwesome.google,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.078,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Already have account? ',
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          InkWell(
                            onTap: this.widget.function,
                            child: Text(
                              'Sign In',
                              style: TextStyle(
                                color: Colors.deepOrangeAccent,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5)
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
