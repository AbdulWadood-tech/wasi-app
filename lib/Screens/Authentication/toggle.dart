import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wasi/Screens/Authentication/sign_in_screen.dart';
import 'package:wasi/Screens/Authentication/sign_up_screen.dart';

class ToggleScreen extends StatefulWidget {
  const ToggleScreen({Key? key}) : super(key: key);

  @override
  _ToggleScreenState createState() => _ToggleScreenState();
}

class _ToggleScreenState extends State<ToggleScreen> {
  bool toggleCheck = false;
  toggleScreen() {
    setState(() {
      toggleCheck = !toggleCheck;
    });
  }

  @override
  Widget build(BuildContext context) {
    return toggleCheck
        ? SignIn(function: () {
            toggleScreen();
          })
        : SignUp(function: () {
            toggleScreen();
          });
  }
}
