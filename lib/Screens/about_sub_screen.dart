import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:wasi/Components/about_row_container.dart';

class AboutSubScreen extends StatelessWidget {
  final String accountType;
  final String businessType;
  final String number;
  final String email;
  final String status;
  final String address;
  final String interestedIn;
  final String gender;
  final String age;
  final String work;
  const AboutSubScreen(
      {Key? key,
      required this.work,
      required this.businessType,
      required this.gender,
      required this.status,
      required this.interestedIn,
      required this.age,
      required this.number,
      required this.accountType,
      required this.email,
      required this.address})
      : super(key: key);

  checkAccount() {
    if (accountType == 'Social Account') {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AboutRowContents(
          iconData: Icons.business_center,
          leading: '',
          title: accountType,
        ),
        checkAccount()
            ? AboutRowContents(
                iconData: Icons.business_center,
                leading: 'Business type ',
                title: businessType,
              )
            : SizedBox(),
        AboutRowContents(
          iconData: Icons.work_outlined,
          leading: 'Work at ',
          title: work,
        ),
        AboutRowContents(
          iconData: Icons.phone,
          leading: '',
          title: number,
        ),
        AboutRowContents(
          leading: '',
          iconData: Icons.email,
          title: email,
        ),
        AboutRowContents(
          leading: '',
          iconData: Entypo.heart,
          title: status,
        ),
        AboutRowContents(
          leading: 'Lives in ',
          iconData: Entypo.home,
          title: address,
        ),
        AboutRowContents(
          leading: 'Interested in ',
          iconData: Icons.wc_sharp,
          title: interestedIn,
        ),
        AboutRowContents(
          leading: 'Gender ',
          iconData: Icons.transgender,
          title: gender,
        ),
        AboutRowContents(
          leading: 'Age ',
          iconData: Icons.support_agent_rounded,
          title: age,
        ),
      ],
    );
  }
}
