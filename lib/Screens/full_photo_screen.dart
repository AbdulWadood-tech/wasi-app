import 'package:flutter/cupertino.dart';

class FullPhotoScreen extends StatelessWidget {
  final String url;
  const FullPhotoScreen({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.network(url),
    );
  }
}
