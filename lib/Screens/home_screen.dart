import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Components/post_container.dart';
import 'package:wasi/Database%20Services/post_services.dart';
import 'package:wasi/Models/post_model.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/post_screen.dart';
import 'package:wasi/State%20Management/profile_manager.dart';
import 'package:wasi/State%20Management/theme_manager.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List posts = [];
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final profileManager = Provider.of<ProfileManager>(context);
    profileManager.getUserData(AuthServices().getUid());
    return Scaffold(
      body: SingleChildScrollView(
        child: StreamBuilder<QuerySnapshot>(
            stream: PostServices().getPostStream(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final postsFromFirebase = snapshot.data!.docs;
                for (var item in postsFromFirebase) {
                  PostModel model = PostModel(
                      description: item.get('Description'),
                      name: item.get('Name'),
                      profileUrl: item.get('Profile Url'),
                      accType: item.get("Account Type"),
                      videoUrl: item.get('Video Url'));
                  posts.add(model);
                }
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PostScreen(
                                profileUrl:
                                    profileManager.userDataModel!.profileUrl,
                                name: profileManager.userDataModel!.name,
                                accType:
                                    profileManager.userDataModel!.accountType);
                          }));
                        },
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: AssetImage('images/profile.jpg'),
                              radius: 23,
                            ),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 7, vertical: 10),
                                padding: EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 15),
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: themeChanger.whiteToBlack,
                                  border: Border.all(
                                    color: Colors.grey.withOpacity(0.3),
                                  ),
                                ),
                                child: Text(
                                  'Create your post',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                            ),
                            Icon(
                              Icons.image,
                              color: Colors.blue,
                              size: 30,
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 70),
                      width: double.infinity,
                      height: 1,
                      color: Colors.grey.withOpacity(0.3),
                    ),
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.symmetric(vertical: 20),
                      shrinkWrap: true,
                      itemCount: 2,
                      itemBuilder: (context, i) {
                        print(posts.length);
                        return PostContainer(
                          description: posts[i].description,
                          profileUrl: posts[i].profileUrl,
                          imageUrl: '',
                          color: themeChanger.whiteToBlack,
                          function: () {},
                          name: posts[i].name,
                        );
                      },
                    ),
                  ],
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      ),
    );
  }
}
