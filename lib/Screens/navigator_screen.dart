import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Components/theme_switch_button.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/Authentication/toggle.dart';
import 'package:wasi/Screens/business_posts.dart';
import 'package:wasi/Screens/nearby_screen.dart';
import 'package:wasi/Screens/profile_screen.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

import 'home_screen.dart';

class NavigatorScreen extends StatefulWidget {
  const NavigatorScreen({Key? key}) : super(key: key);

  @override
  _NavigatorScreenState createState() => _NavigatorScreenState();
}

class _NavigatorScreenState extends State<NavigatorScreen> {
  int currentIndex = 0;
  String? appbarTitle;
  getScreen() {
    if (currentIndex == 0) {
      setState(() {
        appbarTitle = 'Home';
      });

      return HomeScreen();
    } else if (currentIndex == 1) {
      setState(() {
        appbarTitle = 'Business';
      });

      return BusinessPost();
    } else if (currentIndex == 2) {
      setState(() {
        appbarTitle = 'Nearby';
      });

      return NearByScreen();
    } else {
      setState(() {
        appbarTitle = 'Profile';
      });

      return ProfileScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    var isNotPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    var width = MediaQuery.of(context).size.width;
    final themeChanger = Provider.of<ThemeManager>(context);
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 30,
        onTap: (int) {
          setState(() {
            currentIndex = int;
          });
        },
        currentIndex: currentIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            title: Text(
              'Home',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.business_center_rounded,
            ),
            title: Text(
              'Business',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.location_on,
            ),
            title: Text(
              'Nearby',
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            title: Text(
              'Profile',
            ),
          ),
        ],
      ),
      appBar: AppBar(
        elevation: 1,
        leading: Builder(
          builder: (context) => TextButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(
              Icons.menu,
              color: Colors.grey,
            ),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Center(
              child: currentIndex == 0
                  ? Text(
                      'Home',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.red[700],
                          fontSize: 24,
                          fontWeight: FontWeight.bold),
                    )
                  : currentIndex == 1
                      ? Text(
                          'Business',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.red[700],
                              fontSize: 24,
                              fontWeight: FontWeight.bold),
                        )
                      : currentIndex == 2
                          ? Text(
                              'Nearby',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.red[700],
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold),
                            )
                          : Text(
                              'Profile',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.red[700],
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold),
                            ),
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 23.0),
            child: Icon(Icons.message_rounded, color: Colors.grey),
          ),
        ],
      ),
      drawer: Container(
        width: isNotPortrait ? width * 0.7 : width * 0.4,
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
          child: Drawer(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: [
                        DrawerHeader(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "Wasi.",
                                  style: TextStyle(
                                      fontSize: 24.0, color: Color(0xFF0077B5)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 18.0),
                          child: Row(
                            children: [
                              Text(
                                "   Services",
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // _onSelectItem(0);
                            // addToPageList(0);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.business_center,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Business Posts",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // // _onSelectItem(1);
                            // Navigator.pushReplacement(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => UserDashboardMain()));
                          },
                          child: Container(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Entypo.network,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Social Posts",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // _onSelectItem(2);
                            // addToPageList(2);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Nearby",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // _onSelectItem(3);
                            // addToPageList(3);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.post_add_sharp,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "My Posts",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // _onSelectItem(4);
                            // addToPageList(4);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.message,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Inbox",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            // _onSelectItem(5);
                            // addToPageList(5);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 45.0, top: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.settings,
                                  size: 18,
                                  color: Colors.grey,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    "Settings",
                                    style: TextStyle(
                                        fontSize: 13.5,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    Padding(
                      padding: const EdgeInsets.only(left: 18.0),
                      child: Row(
                        children: [
                          Text(
                            "   Change theme",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          SizedBox(width: 15),
                          ThemeSwitchButton(
                            size: false,
                            check: themeChanger.themeValue,
                            function: (val) {
                              setState(() {
                                themeChanger.themeValue = val;
                                themeChanger
                                    .changeTheme(themeChanger.themeValue);
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 80, vertical: 10),
                      child: Container(
                        height: 1,
                        width: double.infinity,
                        color: Colors.grey.withOpacity(0.3),
                      ),
                    ),
                    SizedBox(height: 30),
                    InkWell(
                      onTap: () async {
                        final res = await AuthServices().logOut();
                        if (res) {
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) {
                            return ToggleScreen();
                          }));
                        }
                      },
                      child: Text(
                        'LogOut',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red[800]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      body: getScreen(),
    );
  }
}
