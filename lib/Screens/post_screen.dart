import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wasi/Database%20Services/post_services.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';

class PostScreen extends StatefulWidget {
  final String name;
  final String accType;

  final String profileUrl;
  const PostScreen(
      {Key? key,
      required this.profileUrl,
      required this.name,
      required this.accType})
      : super(key: key);

  @override
  _PostScreenState createState() => _PostScreenState();
}

class _PostScreenState extends State<PostScreen> {
  bool videoCheck = false;
  bool scroll = false;

  String? description;
  File? file;
  var image;
  var _postTypes = [
    "Social post",
    "Business Post",
  ];
  String? _selectedPost;
  @override
  Widget build(BuildContext context) {
    return scroll
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(
                'Create your post',
                style: TextStyle(color: Colors.red),
              ),
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back, color: Colors.grey),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundImage: AssetImage('images/profile.jpg'),
                            radius: 30,
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Abdul Wadood',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 22,
                                  ),
                                ),
                                Container(
                                  height: 30,
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(3),
                                      border: Border.all(
                                          color: Colors.grey.withOpacity(0.3))),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      hint: Text(
                                        'Social Post',
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                      value: _selectedPost,
                                      isDense: true,
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          _selectedPost = newValue;
                                        });
                                      },
                                      items: _postTypes.map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 20),
                      TextField(
                        onChanged: (val) {
                          description = val;
                        },
                        textAlign: TextAlign.start,
                        maxLines: 8,
                        maxLength: 160,
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          labelText: 'What\'s on your mind?',
                          hintText: "What\'s on your mind?",
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Attach file to the post',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      Text(
                        'Video',
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold),
                      ),
                      InkWell(
                        onTap: () async {
                          var video = await PostServices().pickVideo();
                          // var thumbnail = model.videoThumbnail;
                          // var video = model.video;
                          file = File(video.path);

                          // file = File(thumbnail);
                          // setState(() {
                          //   videoCheck = true;
                          //   print('done');
                          // });
                        },
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: Colors.grey.withOpacity(0.3),
                            ),
                          ),
                          child: Icon(
                            Icons.video_collection_rounded,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () async {
                          setState(() {
                            scroll = true;
                          });
                          String url =
                              await PostServices().addVideoToStorage(file!);
                          await PostServices().addPost(
                              AuthServices().getUid(),
                              this.widget.profileUrl,
                              this.widget.accType,
                              this.widget.name,
                              description!,
                              url);
                          Navigator.pop(context);
                        },
                        child: Text('Post'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
