import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Components/PhotoContainer.dart';
import 'package:wasi/Components/custom_text_field.dart';
import 'package:wasi/Components/dropdown_button.dart';
import 'package:wasi/Components/tabbar_component.dart';
import 'package:wasi/Database%20Services/database_services.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/navigator_screen.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

class ProfileFormScreen extends StatefulWidget {
  final String email;
  const ProfileFormScreen({Key? key, required this.email}) : super(key: key);

  @override
  _ProfileFormScreenState createState() => _ProfileFormScreenState();
}

class _ProfileFormScreenState extends State<ProfileFormScreen> {
  var _businessTypes = [
    "Developer",
    "Programmer",
    "Youtuber",
    "Graphic designer",
    "Sales Man",
    "Web Developer",
  ];
  var _gender = ['Male', 'Female', 'Others'];
  var _work = ['Student', 'Wasi', 'Freelancer', 'Facebook'];
  var _interest = ['Male', 'Female'];
  var _status = ['Single', 'Engaged', 'Married', 'Divorced'];
  var _selectedBusinessType;
  var _selectedWork;
  var _selectedGender;
  var _selectedStatus;
  var _selectedInterest;
  int businessCheck = 0;

  bool loading = false;

  //data to be post
  bool reloadProfile = false;
  bool reloadCover = false;
  String? profileUrl;
  String? coverUrl;
  var profileAsset;
  var coverAsset;

  String? fullName;
  String? age;
  String? number;
  String? address;
  String accountType = 'Business Account';
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    return loading
        ? Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Setting up your profile please wait',
                    style: TextStyle(color: Colors.grey, fontSize: 9),
                  )
                ],
              ),
            ),
          )
        : DefaultTabController(
            length: 2,
            child: Scaffold(
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 30, horizontal: 30),
                    child: Column(
                      children: [
                        Text(
                          'Please complete your profile',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 20),
                        ),
                        SizedBox(height: 20),
                        PhotoContainer(
                          photoCheck: reloadProfile,
                          profileUrl: profileAsset,
                          title: 'Add a profile photo',
                          check: true,
                          onTap: () async {
                            profileAsset =
                                await DatabaseServices().selectFile();
                            setState(() {
                              if (profileAsset != null) {
                                reloadProfile = true;
                              }
                            });
                          },
                        ),
                        PhotoContainer(
                          photoCheck: reloadCover,
                          profileUrl: coverAsset,
                          title: 'Add a Cover photo',
                          check: false,
                          onTap: () async {
                            coverAsset = await DatabaseServices().selectFile();
                            setState(() {
                              if (coverAsset != null) {
                                reloadCover = true;
                              }
                            });
                          },
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Please enter your full name',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            CustomTextField(
                              onChanged: (val) {
                                fullName = val;
                              },
                              hint: 'Full name',
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Please enter your age',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            CustomTextField(
                              onChanged: (val) {
                                age = val;
                              },
                              hint: 'age',
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Please enter your phone number',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            CustomTextField(
                              onChanged: (val) {
                                number = val;
                              },
                              hint: 'Phone number',
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Please enter your Address',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            CustomTextField(
                              onChanged: (val) {
                                address = val;
                              },
                              hint: 'Address',
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            'What type of account do you want to create?',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 10),
                        TabBarComponents(
                            color: themeChanger.tabBarColor!,
                            onTap: (index) {
                              print(index);
                              setState(() {
                                if (index == 0) {
                                  accountType = 'Business Account';
                                } else if (index == 1) {
                                  accountType = 'Social Account';
                                }
                                businessCheck = index;
                              });
                            },
                            tab1Title: 'Business account',
                            tab3Title: 'Social Account'),
                        SizedBox(height: 30),
                        businessCheck == 0
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      'What type of business do you have?',
                                      style: TextStyle(
                                          color: Colors.red,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    decoration: BoxDecoration(
                                        color: themeChanger.textFieldColor,
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(
                                            color:
                                                Colors.grey.withOpacity(0.3))),
                                    child: FormField<String>(
                                      builder: (FormFieldState<String> state) {
                                        return InputDecorator(
                                          decoration: InputDecoration(
                                            // labelStyle: textStyle,
                                            errorStyle: TextStyle(
                                                color: Colors.redAccent,
                                                fontSize: 16.0),
                                            border: InputBorder.none,
                                          ),
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton<String>(
                                              value: _selectedBusinessType,
                                              isDense: true,
                                              onChanged: (String? newValue) {
                                                setState(() {
                                                  _selectedBusinessType =
                                                      newValue!;
                                                  state.didChange(newValue);
                                                });
                                              },
                                              items: _businessTypes
                                                  .map((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(
                                                    value,
                                                    style: TextStyle(
                                                        color: Colors.blue),
                                                  ),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox(),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'What do you do?',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                  color: themeChanger.textFieldColor,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3))),
                              child: FormField<String>(
                                builder: (FormFieldState<String> state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      // labelStyle: textStyle,
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        value: _selectedWork,
                                        isDense: true,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedWork = newValue;
                                            state.didChange(newValue);
                                          });
                                        },
                                        items: _work.map((String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style:
                                                  TextStyle(color: Colors.blue),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Select your gender',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                  color: themeChanger.textFieldColor,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3))),
                              child: FormField<String>(
                                builder: (FormFieldState<String> state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      // labelStyle: textStyle,
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        value: _selectedGender,
                                        isDense: true,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedGender = newValue;
                                            state.didChange(newValue);
                                          });
                                        },
                                        items: _gender.map((String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style:
                                                  TextStyle(color: Colors.blue),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Status',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                  color: themeChanger.textFieldColor,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3))),
                              child: FormField<String>(
                                builder: (FormFieldState<String> state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      // labelStyle: textStyle,
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        value: _selectedStatus,
                                        isDense: true,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedStatus = newValue;
                                            state.didChange(newValue);
                                          });
                                        },
                                        items: _status.map((String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style:
                                                  TextStyle(color: Colors.blue),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Interested In',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              decoration: BoxDecoration(
                                  color: themeChanger.textFieldColor,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3))),
                              child: FormField<String>(
                                builder: (FormFieldState<String> state) {
                                  return InputDecorator(
                                    decoration: InputDecoration(
                                      // labelStyle: textStyle,
                                      errorStyle: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 16.0),
                                      border: InputBorder.none,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                        value: _selectedInterest,
                                        isDense: true,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedInterest = newValue;
                                            state.didChange(newValue);
                                          });
                                        },
                                        items: _interest.map((String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style:
                                                  TextStyle(color: Colors.blue),
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 30),
                        InkWell(
                          onTap: () async {
                            try {
                              setState(() {
                                loading = true;
                              });
                              File file1 = File(profileAsset.path);
                              File file2 = File(coverAsset.path);
                              String pUrl = await DatabaseServices()
                                  .addPhotoTStorage(file1);
                              String cUrl = await DatabaseServices()
                                  .addPhotoTStorage(file2);
                              await DatabaseServices().addUser(
                                pUrl,
                                cUrl,
                                fullName!,
                                this.widget.email,
                                age!,
                                number!,
                                address!,
                                accountType!,
                                _selectedBusinessType ??
                                    'Not a business account',
                                _selectedGender,
                                _selectedStatus,
                                _selectedInterest,
                                AuthServices().getUid(),
                                _selectedWork,
                              );
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return NavigatorScreen();
                              }));
                            } catch (e) {
                              setState(() {
                                loading = false;
                              });
                            }
                          },
                          child: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(vertical: 15),
                            decoration: BoxDecoration(
                              color: Colors.red[700],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Text(
                              'Continue to application',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
  }
}
