import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Database%20Services/database_services.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/about_sub_screen.dart';
import 'package:wasi/Screens/full_photo_screen.dart';
import 'package:wasi/Screens/settings.dart';
import 'package:wasi/State%20Management/theme_manager.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int selectedScreen = 0;
  bool scrollingP = false;
  bool scrollingC = false;

  checkAccountForBusiness(String type) {
    if (type == 'Social Account') {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            child: StreamBuilder<DocumentSnapshot>(
                stream: DatabaseServices().getUserData(AuthServices().getUid()),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var data = snapshot.data;
                    return Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: 255,
                          child: Stack(
                            children: [
                              Positioned(
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  child: scrollingC
                                      ? Center(
                                          child: CircularProgressIndicator(),
                                        )
                                      : InkWell(
                                          onTap: () {
                                            Navigator.push(context,
                                                MaterialPageRoute(
                                                    builder: (context) {
                                              return FullPhotoScreen(
                                                  url: data?.get('Cover Url'));
                                            }));
                                          },
                                          child: Container(
                                            width: double.infinity,
                                            height: 180,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: NetworkImage(
                                                      data?.get('Cover Url')),
                                                  fit: BoxFit.cover,
                                                ),
                                                color: Colors.grey,
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                          ),
                                        )),
                              Positioned(
                                  bottom: 0,
                                  left: 110,
                                  child: scrollingP
                                      ? Center(
                                          child: CircularProgressIndicator())
                                      : InkWell(
                                          onTap: () {
                                            Navigator.push(context,
                                                MaterialPageRoute(
                                                    builder: (context) {
                                              return FullPhotoScreen(
                                                  url:
                                                      data?.get('Profile Url'));
                                            }));
                                          },
                                          child: Container(
                                            height: 140,
                                            width: 140,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                  image: NetworkImage(
                                                      data?.get('Profile Url')),
                                                  fit: BoxFit.cover,
                                                ),
                                                border: Border.all(
                                                    color: Colors.white,
                                                    width: 3),
                                                borderRadius:
                                                    BorderRadius.circular(90),
                                                color: Colors.grey),
                                          ),
                                        )),
                              Positioned(
                                  bottom: 80,
                                  right: 10,
                                  child: InkWell(
                                    onTap: () async {
                                      setState(() {
                                        scrollingC = true;
                                      });
                                      var image =
                                          await DatabaseServices().selectFile();
                                      if (image != null) {
                                        File file = File(image.path);
                                        String url = await DatabaseServices()
                                            .addPhotoTStorage(file);
                                        DatabaseServices().changeCoverPicture(
                                            url, AuthServices().getUid());
                                      }
                                      setState(() {
                                        scrollingC = false;
                                      });
                                    },
                                    child: CircleAvatar(
                                      backgroundColor:
                                          Colors.white.withOpacity(0.5),
                                      radius: 15,
                                      child: Icon(Icons.camera_alt_outlined),
                                    ),
                                  )),
                              Positioned(
                                  bottom: 12,
                                  left: 220,
                                  child: InkWell(
                                    onTap: () async {
                                      setState(() {
                                        scrollingP = true;
                                      });
                                      var image =
                                          await DatabaseServices().selectFile();
                                      if (image != null) {
                                        File file = File(image.path);
                                        String url = await DatabaseServices()
                                            .addPhotoTStorage(file);
                                        DatabaseServices().changeProfilePicture(
                                            url, AuthServices().getUid());
                                      }
                                      setState(() {
                                        scrollingP = false;
                                      });
                                    },
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 15,
                                      child: Icon(Icons.camera_alt_outlined),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          data?.get('Name'),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        checkAccountForBusiness(data?.get('Account Type'))
                            ? SizedBox(height: 25)
                            : SizedBox(),
                        checkAccountForBusiness(data?.get('Account Type'))
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                      color: Color(0xff27AE60),
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 15,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Text('Open for Business'),
                                ],
                              )
                            : SizedBox(),
                        SizedBox(height: 3),
                        checkAccountForBusiness(data?.get('Account Type'))
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                      color: Color(0xff27AE60),
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 15,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Text('Virtual Consultation'),
                                ],
                              )
                            : SizedBox(),
                        SizedBox(height: 35),
                        Container(
                          height: 40,
                          decoration: BoxDecoration(
                              color: themeChanger.tabBarColor,
                              borderRadius: BorderRadius.circular(20)),
                          child: TabBar(
                            unselectedLabelColor: Colors.blue,
                            indicator: BoxDecoration(
                                color: Color(0xff00A0DC),
                                borderRadius: BorderRadius.circular(20)),
                            onTap: (i) {
                              setState(() {
                                selectedScreen = i;
                              });
                            },
                            tabs: [
                              Tab(
                                text: "About",
                              ),
                              Tab(
                                text: "Posts",
                              ),
                              Tab(
                                text: "Settings",
                              ),
                            ],
                          ),
                        ),
                        Builder(builder: (context) {
                          if (selectedScreen == 0) {
                            return AboutSubScreen(
                              businessType: data?.get('Business Type'),
                              gender: data?.get('Gender'),
                              status: data?.get('Status'),
                              interestedIn: data?.get('Interested In'),
                              age: data?.get('Age'),
                              number: data?.get('Number'),
                              accountType: data?.get('Account Type'),
                              email: data?.get('Email'),
                              address: data?.get('Address'),
                              work: data?.get('Work'),
                            );
                          } else if (selectedScreen == 1) {
                            return Text('Working on it');
                          } else {
                            return ProfileSettings(
                              businessType: data?.get('Business Type'),
                              gender: data?.get('Gender'),
                              status: data?.get('Status'),
                              interestedIn: data?.get('Interested In'),
                              age: data?.get('Age'),
                              number: data?.get('Number'),
                              accountType: data?.get('Account Type'),
                              email: data?.get('Email'),
                              address: data?.get('Address'),
                              work: data?.get('Work'),
                            );
                          }
                        })
                      ],
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }),
          ),
        ),
      ),
    );
  }
}
