import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wasi/Components/custom_dialoge_box.dart';
import 'package:wasi/Components/custom_dividerr.dart';
import 'package:wasi/Components/custom_text_field.dart';
import 'package:wasi/Components/settings_row.dart';
import 'package:wasi/Components/tabbar_component.dart';
import 'package:wasi/Components/update_button.dart';
import 'package:wasi/Database%20Services/database_services.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';

class ProfileSettings extends StatefulWidget {
  final String accountType;
  final String businessType;
  final String number;
  final String email;
  final String status;
  final String address;
  final String interestedIn;
  final String gender;
  final String age;
  final String work;
  const ProfileSettings(
      {Key? key,
      required this.work,
      required this.businessType,
      required this.gender,
      required this.status,
      required this.interestedIn,
      required this.age,
      required this.number,
      required this.accountType,
      required this.email,
      required this.address})
      : super(key: key);

  @override
  State<ProfileSettings> createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {
  bool accCheck = false;
  bool businessCheck = false;
  bool workCheck = false;
  bool emailCheck = false;
  bool numberCheck = false;
  bool genderCheck = false;
  bool interestCheck = false;
  bool ageCheck = false;
  bool statusCheck = false;
  bool addressCheck = false;
  bool nameCheck = false;
  checkAccount() {
    if (widget.accountType == 'Social Account') {
      return false;
    } else {
      return true;
    }
  }

  var _gender = ['Male', 'Female', 'Others'];
  var _interest = ['Male', 'Female'];
  var _status = ['Single', 'Engaged', 'Married', 'Divorced'];
  String? _selectedBusinessType;
  String? _selectedWork;
  var _selectedGender;
  var _selectedStatus;
  var _selectedInterest;
  //data to be post

  String? fullName;
  String? age;
  String? number;
  String? address;
  String accountUpdate = 'Business Account';
  String? email;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Account Type',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            accCheck
                ? Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Container(
                          child: TabBarComponents(
                            color: Colors.white,
                            onTap: (index) {
                              if (index == 0) {
                                accountUpdate = 'Business Account';
                              } else if (index == 1) {
                                accountUpdate = 'Social Account';
                              }
                            },
                            tab1Title: 'Business account',
                            tab3Title: 'Social Account',
                          ),
                          height: 30,
                        ),
                        SaveButton(
                          onTap: () {
                            DatabaseServices().changeFieldInProfileSettings(
                                AuthServices().getUid(),
                                'Account Type',
                                accountUpdate);
                            setState(() {
                              accCheck = false;
                            });
                          },
                        ),
                      ],
                    ),
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        accCheck = true;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.accountType,
                    subTitle: 'Account type ',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
            Text(
              'Business Information',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            businessCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'Business type',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.blue),
                          ),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                _selectedBusinessType = val;
                              },
                              hint: 'Change your business type',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(),
                            'Business Type',
                            _selectedBusinessType!);
                        setState(() {
                          businessCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        businessCheck = true;
                        accCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.businessType,
                    subTitle: 'Business type ',
                    icon: Icons.business_center,
                  ),
            workCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'Work at',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.blue),
                          ),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                _selectedWork = val;
                              },
                              hint: 'Change your Work',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(), 'Work', _selectedWork!);
                        setState(() {
                          workCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        workCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.work,
                    subTitle: 'Work at ',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
            Text(
              'Contact Info',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            emailCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'Email address',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.blue),
                          ),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                email = val;
                              },
                              hint: 'Change your email address',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(), 'Email', email!);
                        setState(() {
                          emailCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        emailCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.email,
                    subTitle: '',
                    icon: Icons.business_center,
                  ),
            numberCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'Phone number',
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.blue),
                          ),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                number = val;
                              },
                              hint: 'Change your phone number',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(), 'Number', number!);
                        setState(() {
                          numberCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        numberCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.number,
                    subTitle: '',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
            Text(
              'Basic Info',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            genderCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Gender',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: Colors.grey.withOpacity(0.3))),
                            child: FormField<String>(
                              builder: (FormFieldState<String> state) {
                                return InputDecorator(
                                  decoration: InputDecoration(
                                    // labelStyle: textStyle,
                                    errorStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontSize: 16.0),
                                    border: InputBorder.none,
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      value: _selectedGender,
                                      isDense: true,
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          _selectedGender = newValue;
                                          state.didChange(newValue);
                                        });
                                      },
                                      items: _gender.map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(),
                            'Gender',
                            _selectedGender!);
                        setState(() {
                          genderCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        genderCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.gender,
                    subTitle: 'Gender ',
                    icon: Icons.business_center,
                  ),
            interestCheck
                ? Column(
                    children: [
                      SizedBox(height: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Interested in',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: Colors.grey.withOpacity(0.3))),
                            child: FormField<String>(
                              builder: (FormFieldState<String> state) {
                                return InputDecorator(
                                  decoration: InputDecoration(
                                    // labelStyle: textStyle,
                                    errorStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontSize: 16.0),
                                    border: InputBorder.none,
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      value: _selectedInterest,
                                      isDense: true,
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          _selectedInterest = newValue;
                                          state.didChange(newValue);
                                        });
                                      },
                                      items: _interest.map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(),
                            'Interested In',
                            _selectedInterest!);
                        setState(() {
                          interestCheck = false;
                        });
                      })
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        interestCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.interestedIn,
                    subTitle: 'Interested in ',
                    icon: Icons.business_center,
                  ),
            ageCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text('Age',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w600)),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                age = val;
                              },
                              hint: 'Change your Age',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(), 'Age', age!);
                        setState(() {
                          ageCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        ageCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        statusCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.age,
                    subTitle: 'Age ',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
            Text(
              'Relationship status',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            statusCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'Status',
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: Colors.grey.withOpacity(0.3))),
                            child: FormField<String>(
                              builder: (FormFieldState<String> state) {
                                return InputDecorator(
                                  decoration: InputDecoration(
                                    // labelStyle: textStyle,
                                    errorStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontSize: 16.0),
                                    border: InputBorder.none,
                                  ),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      value: _selectedStatus,
                                      isDense: true,
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          _selectedStatus = newValue;
                                          state.didChange(newValue);
                                        });
                                      },
                                      items: _status.map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(),
                            'Status',
                            _selectedStatus!);
                        setState(() {
                          statusCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        statusCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        addressCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.status,
                    subTitle: '',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
            Text(
              'Placed lived',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            addressCheck
                ? Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            'address',
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                          ),
                          Container(
                            height: 50,
                            child: CustomTextField(
                              onChanged: (val) {
                                address = val;
                              },
                              hint: 'Change your address',
                            ),
                          ),
                        ],
                      ),
                      SaveButton(onTap: () {
                        DatabaseServices().changeFieldInProfileSettings(
                            AuthServices().getUid(), 'Address', address!);
                        setState(() {
                          addressCheck = false;
                        });
                      }),
                    ],
                  )
                : SettingRow(
                    function: () {
                      setState(() {
                        addressCheck = true;
                        accCheck = false;
                        businessCheck = false;
                        workCheck = false;
                        emailCheck = false;
                        numberCheck = false;
                        genderCheck = false;
                        interestCheck = false;
                        ageCheck = false;
                        statusCheck = false;
                        nameCheck = false;
                      });
                    },
                    title: widget.address,
                    subTitle: 'Lived in ',
                    icon: Icons.business_center,
                  ),
            CustomDivider(),
          ],
        ),
      ),
    );
  }
}
