import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wasi/Database%20Services/database_services.dart';
import 'package:wasi/Screens/Authentication/Auth%20Services/auth_services.dart';
import 'package:wasi/Screens/Authentication/toggle.dart';
import 'package:wasi/Screens/navigator_screen.dart';
import 'package:wasi/Screens/profile_form_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool? check;
  @override
  void initState() {
    super.initState();
    checkUser();
    Timer(Duration(seconds: 2), () {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return AuthServices().getUser() && check!
            ? NavigatorScreen()
            : AuthServices().getUser() && check == false
                ? ProfileFormScreen(email: AuthServices().getEmail())
                : ToggleScreen();
      }));
    });
  }

  checkUser() async {
    check = await DatabaseServices().checkIfDocExists(AuthServices().getUid());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset(
          'images/logo.jpg',
          height: 100,
        ),
      ),
    );
  }
}
