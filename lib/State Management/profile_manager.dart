import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:wasi/Models/user_data_model.dart';

class ProfileManager extends ChangeNotifier {
  //reference for users
  final CollectionReference userReference =
      FirebaseFirestore.instance.collection('Users');

  //user model
  UserDataModel? userDataModel;

  Future getUserData(String uid) {
    return userReference.doc(uid).get().then((DocumentSnapshot value) {
      userDataModel = UserDataModel(
          accountType: value.get('Account Type'),
          profileUrl: value.get('Profile Url'),
          name: value.get('Name'));
      notifyListeners();
    });
  }
}
