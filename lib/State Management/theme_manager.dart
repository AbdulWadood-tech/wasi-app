import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeManager extends ChangeNotifier {
  var themeMode = ThemeMode.light;
  Color textFieldColor = Colors.white;
  Color textColor = Colors.grey;
  Color borderColor = Colors.grey;
  Color headingColors = Colors.black;
  Color? tabBarColor = Colors.grey[200];
  bool themeValue = true;
  Color whiteToBlack = Colors.white;

  void changeTheme(bool check) {
    if (check == false) {
      themeMode = ThemeMode.dark;
      themeValue = false;
      textFieldColor = Color(0xff262626);
      textColor = Colors.grey.withOpacity(0.5);
      borderColor = Colors.black;
      headingColors = Colors.white;
      tabBarColor = Colors.grey[700];
      whiteToBlack = Colors.black;
      notifyListeners();
    } else {
      themeMode = ThemeMode.light;
      textFieldColor = Colors.white;
      themeValue = true;
      borderColor = Colors.grey;
      textColor = Colors.grey;
      headingColors = Colors.black;
      tabBarColor = Colors.grey[200];
      whiteToBlack = Colors.white;
      notifyListeners();
    }
  }
}
