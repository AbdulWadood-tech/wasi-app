import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:wasi/Screens/navigator_screen.dart';
import 'package:wasi/Screens/profile_form_screen.dart';
import 'package:wasi/State%20Management/profile_manager.dart';

import 'Screens/splash_screen.dart';
import 'State Management/theme_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<ThemeManager>(
            create: (_) => ThemeManager(),
          ),
          ChangeNotifierProvider<ProfileManager>(
            create: (_) => ProfileManager(),
          ),
        ],
        child: Builder(
          builder: (context) {
            final themeChanger = Provider.of<ThemeManager>(context);
            return MaterialApp(
              themeMode: themeChanger.themeMode,
              darkTheme: ThemeData(
                  scaffoldBackgroundColor: Colors.black,
                  textTheme: TextTheme(
                    bodyText2: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  appBarTheme: AppBarTheme(
                    color: Colors.black,
                  ),
                  canvasColor: Colors.black),
              theme: ThemeData(
                appBarTheme: AppBarTheme(
                  color: Colors.white,
                ),
                textTheme: TextTheme(
                  bodyText2: TextStyle(
                    color: Colors.black,
                  ),
                ),
                scaffoldBackgroundColor: Colors.white,
              ),
              title: 'Wasi App',
              debugShowCheckedModeBanner: false,
              home: SplashScreen(),
            );
          },
        ));
  }
}
